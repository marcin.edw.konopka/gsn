# Controlling crowd density during pandemic
 
This is repository containing code for project realised during *[GUT] Deep neural networks for data* analysis course.

**Data**

1. [Shanghaitech Datasets](https://www.kaggle.com/tthien/shanghaitech)

2. [CrowdMask (our own)](https://drive.google.com/drive/folders/1VCv_Pt_wSFTITTsNZtxCA2oHo_98mfkZ?usp=sharing)

3. [CrowdMask preprocessed](https://drive.google.com/file/d/1tPvBDjPvc13LDo8usbc62lXu42vZiO-D/view?usp=sharing)


## Acknowledgements

1. [princenarula222/Crowd_Annotation](https://github.com/princenarula222/Crowd_Annotation).

2. [ZhengPeng7/Multi_column_CNN_in_Keras](https://github.com/ZhengPeng7/Multi_column_CNN_in_Keras)

3. [gjy3035/Awesome-Crowd-Counting](https://github.com/gjy3035/Awesome-Crowd-Counting)

4. [Single-Image Crowd Countingvia Multi-Column Convolutional Neural Network}. Shanghaitech University, 2016.](https://people.eecs.berkeley.edu/~yima/psfile/Single-Image-Crowd-Counting.pdf)