import gradio as gr
import cv2
import numpy as np
import matplotlib.pyplot as plt

from keras.models import load_model
from misc.utils import monitor_mae, monitor_mse

net1 = load_model('./weights/mae_11.63_mcnn_all_masks_val.hdf5', custom_objects={
                   'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})
net2 = load_model('./weights/mae_14.89_mcnndsh_all_masks_val.hdf5', custom_objects={
                   'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})
net3 = load_model('./weights/mae_18.48_mcnnds_all_masks_val.hdf5', custom_objects={
                   'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})
net4 = load_model('./weights/mae_54.41_test_with_l2_shhb_val.hdf5', custom_objects={
                   'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})
net5 = load_model('./weights/MCNN-weights_107-0.00010596_0.00001225.hdf5', custom_objects={
                   'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})

def cc(img, model):
    img = (img - 127.5) / 128
    inputs = np.reshape(img, [1, *img.shape[:2], 1])
    
    if model == 'MCNN':
        outputs = np.squeeze(net1.predict(inputs))
    elif model == 'MCNN-DSH':
        outputs = np.squeeze(net2.predict(inputs))
    elif model == 'MCNN-DSF':
        outputs = np.squeeze(net3.predict(inputs))
    elif model == 'MCNN-L2reg':
        outputs = np.squeeze(net4.predict(inputs))
    elif model == 'SHHB':
        outputs = np.squeeze(net5.predict(inputs))
        
    out_img = np.squeeze(inputs)
    out_den = np.asarray(outputs * (255 / (np.max(outputs) - np.min(outputs))), dtype=np.uint8)
    count = f'{round(np.sum(outputs))}'
    
    return out_den, count

iface = gr.Interface(
    cc,
    [gr.inputs.Image(image_mode="L"), gr.inputs.Radio(["MCNN", "MCNN-DSH", "MCNN-DSF", "MCNN-L2reg", "SHHB"])],
    [gr.outputs.Image(label="Density map"), gr.outputs.Textbox(label="Crowd prediction")],
    title="Pandemic crowd counter")

# iface.launch(share=True)
iface.launch()