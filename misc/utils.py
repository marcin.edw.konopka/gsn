import os
import cv2
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt
from keras import backend as K
from keras.losses import mean_squared_error
from keras.models import load_model
from sklearn.utils import shuffle

DATA_PATH = '/datasets/crowd'

def gen_imgPaths_and_labelPaths(dataset="B"):
    train_path = DATA_PATH + '/ShanghaiTech/formatted_trainval/shanghaitech_part_' + dataset + '_patches_9/train/'
    train_den_path = DATA_PATH + '/ShanghaiTech/formatted_trainval/shanghaitech_part_' + dataset + '_patches_9/train_den/'
    
    val_path = DATA_PATH + '/ShanghaiTech/formatted_trainval/shanghaitech_part_' + dataset + '_patches_9/val/'
    val_den_path = DATA_PATH + '/ShanghaiTech/formatted_trainval/shanghaitech_part_' + dataset + '_patches_9/val_den/'
    
    img_path = DATA_PATH + '/ShanghaiTech/original/part_' + dataset + '/test_data/images/'
    den_path = DATA_PATH + '/ShanghaiTech/original/part_' + dataset + '/test_data/ground-truth_csv/'
    
    img2_path = DATA_PATH + '/ShanghaiTech/original/part_' + dataset + '/test2_data/images/'
    den2_path = DATA_PATH + '/ShanghaiTech/original/part_' + dataset + '/test2_data/ground-truth_csv/'
    
    print(img_path)
    
    train_paths = sorted([train_path + p for p in os.listdir(train_path)], key=lambda x: float(x[:-len('.jpg')].split('/')[-1].replace('_', '.')))
    train_labels = sorted([train_den_path + p for p in os.listdir(train_den_path)], key=lambda x: float(x[:-len('.jpg')].split('/')[-1].replace('_', '.')))
    
    validation_paths = sorted([val_path + p for p in os.listdir(val_path)], key=lambda x: float(x[:-len('.jpg')].split('/')[-1].replace('_', '.')))
    validation_labels = sorted([val_den_path + p for p in os.listdir(val_den_path)], key=lambda x: float(x[:-len('.jpg')].split('/')[-1].replace('_', '.')))
    
    test_paths = sorted([img_path + p for p in os.listdir(img_path)], key=lambda x: int(x[:-len('.jpg')].split('/')[-1].split('_')[1]))
    test_labels = sorted([den_path + p for p in os.listdir(den_path)], key=lambda x: int(x[:-len('.jpg')].split('/')[-1].split('_')[1]))
    
    test2_paths = sorted([img2_path + p for p in os.listdir(img2_path)], key=lambda x: int(x[:-len('.jpg')].split('/')[-1].split('_')[1]))
    test2_labels = sorted([den2_path + p for p in os.listdir(den2_path)], key=lambda x: int(x[:-len('.jpg')].split('/')[-1].split('_')[1]))

    return train_paths, train_labels, validation_paths, validation_labels, test_paths, test_labels, test2_paths, test2_labels


def generate_generator(img_paths, label_paths, batch_size=32, is_shuffle=False, img_flip=0):
    flag_continue = 0
    idx_total = 0
    img_paths = np.squeeze(img_paths).tolist() if isinstance(img_paths, np.ndarray) else img_paths
    label_paths = np.squeeze(label_paths).tolist() if isinstance(label_paths, np.ndarray) else label_paths
    if is_shuffle:
        paths_shuffled = shuffle(np.hstack([np.asarray(img_paths).reshape(-1, 1), np.asarray(label_paths).reshape(-1, 1)]))
        img_paths, label_paths = np.squeeze(paths_shuffled[:, 0]).tolist(), np.squeeze(paths_shuffled[:, 1]).tolist()
    data_len = len(label_paths)
    while True:
        if not flag_continue:
            x = []
            y = []
            inner_iter_num = batch_size
        else:
            idx_total = 0
            inner_iter_num = batch_size - data_len % batch_size
        for _ in range(inner_iter_num):
            if idx_total >= data_len:
                flag_continue = 1
                break
            else:
                flag_continue = 0
            img = (cv2.imread(img_paths[idx_total], 0) - 127.5) / 128
            density_map = np.loadtxt(label_paths[idx_total], delimiter=',')
            stride = 4
            density_map_quarter = np.zeros((np.asarray(density_map.shape).astype(int)//stride).tolist())
            for r in range(density_map_quarter.shape[0]):
                for c in range(density_map_quarter.shape[1]):
                    density_map_quarter[r, c] = np.sum(density_map[r*stride:(r+1)*stride, c*stride:(c+1)*stride])
            x.append(img.reshape(*img.shape, 1))
            y.append(density_map_quarter.reshape(*density_map_quarter.shape, 1))
            if img_flip:
                pass
            idx_total += 1
        if not flag_continue:
            x, y = np.asarray(x), np.asarray(y)
            yield x, y


def monitor_mae(labels, preds):
    return K.sum(K.abs(labels - preds)) / 1


def monitor_mse(labels, preds):
    return K.sum(K.square(labels - preds)) / 1


def sample_predictions(X_test, y_test, model, num_test):
    ae = []
    se = []
    for i in random.sample(range(X_test.shape[0]), num_test):
        inputs = np.reshape(X_test[i], [1, *X_test[i].shape[:2], 1])
        outputs = np.squeeze(model.predict(inputs))
        density_map = np.squeeze(y_test[i])
        count_label = np.sum(density_map)
        count_prediction = np.sum(outputs)
        fg, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(16, 5))
        plt.suptitle(' '.join([
            'count_label:', str(round(count_label, 3)),
            'count_prediction:', str(round(count_prediction, 3))
        ]))
        ax0.imshow(np.squeeze(inputs), cmap='gray')
        ax1.imshow(density_map * (255 / (np.max(density_map) - np.min(density_map))))
        ax2.imshow(outputs * (255 / (np.max(outputs) - np.min(outputs))))
        plt.show()
        

def first_predictions(X_test, y_test, model, num_test):
    ae = []
    se = []
    for i in range(X_test.shape[0])[1:num_test+1]:
        inputs = np.reshape(X_test[i], [1, *X_test[i].shape[:2], 1])
        outputs = np.squeeze(model.predict(inputs))
        density_map = np.squeeze(y_test[i])
        count_label = np.sum(density_map)
        count_prediction = np.sum(outputs)
        fg, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(16, 5))
        plt.suptitle(' '.join([
            'count_label:', str(round(count_label, 3)),
            'count_prediction:', str(round(count_prediction, 3))
        ]))
        ax0.imshow(np.squeeze(inputs), cmap='gray')
        ax1.imshow(density_map * (255 / (np.max(density_map) - np.min(density_map))))
        ax2.imshow(outputs * (255 / (np.max(outputs) - np.min(outputs))))
        plt.show() 
        

def calc_mae_mse(X_test, y_test, model):
    ae = []
    se = []
    for i in range(X_test.shape[0]):
        inputs = np.reshape(X_test[i], [1, *X_test[i].shape[:2], 1])
        outputs = np.squeeze(model.predict(inputs))
        density_map = np.squeeze(y_test[i])
        count_label = np.sum(density_map)
        count_prediction = np.sum(outputs)
        ae.append(abs(count_label - count_prediction))
        se.append((count_label - count_prediction) ** 2)
    mae = np.mean(ae)
    mse = np.sqrt(np.mean(se))
    print('MAE:', np.round(mae,2), 'RMSE:', np.round(mse,2))
    
    
def get_avg_count(y_test, model):
    ae = []
    for i in range(y_test.shape[0]):
        density_map = np.squeeze(y_test[i])
        count_label = np.sum(density_map)
        ae.append(count_label)
        se.append((count_label - count_prediction) ** 2)
    mae = np.mean(ae)
    mse = np.sqrt(np.mean(se))
    print('MAE:', mae, 'RMSE:', mse)
    
    
def get_profile(path):
    # Model profile; ref: https://stackoverflow.com/questions/49525776/how-to-calculate-a-mobilenet-flops-in-keras
 
    model = load_model(path, custom_objects={'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})
    session = tf.compat.v1.Session()
    graph = tf.compat.v1.get_default_graph()

    with graph.as_default():
        with session.as_default():
            model = load_model(path, custom_objects={'monitor_mae': monitor_mae, 'monitor_mse': monitor_mse})
            run_meta = tf.compat.v1.RunMetadata()

            opts = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()
            flops = tf.compat.v1.profiler.profile(graph=graph,
                                                  run_meta=run_meta, cmd='op', options=opts)
    tf.compat.v1.reset_default_graph()
    FLOPS = flops.total_float_ops

    print('\nProfile:')
    print('---------')
    print(f'FLOPS: {FLOPS:,}')
    print(f'Mult-Adds: {int(FLOPS/2):,}')
    print(f'Trainsable params: {model.count_params():,}')